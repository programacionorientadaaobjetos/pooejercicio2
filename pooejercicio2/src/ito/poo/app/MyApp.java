package ito.poo.app;

import java.util.Scanner;

public class MyApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		float calificaciones[]= new float[8];
		float suma_reprobados=0 ,suma_dificultades=0,suma_aprobados;
		int conteo_reprobados =0,conteo_dificultades=0,conteo_aprobados=0;
		int porcentaje1 =0;
		
		System.out.println("Guarda las calificaciones en el arreglo  ");
		
		for(int i=0;i<8;i++) {
			System.out.print("ingresa la calificacion "+(i+1));
			calificaciones[i]=entrada.nextFloat();
			
			if(calificaciones[i]<70) {
				conteo_reprobados++;
			}
			
			else if (calificaciones[i]>=85) {
				conteo_aprobados++;
			}
			
			else if (calificaciones[i]>=70 && calificaciones[i] <85) {
				conteo_dificultades++;
			}
		
		
		}
		System.out.println("APROBADOS     : " + (conteo_aprobados * 100)/(conteo_aprobados+conteo_reprobados+conteo_dificultades ) + "%");
		System.out.println("REPROBADOS    : " + (conteo_reprobados * 100)/(conteo_reprobados+conteo_aprobados+conteo_dificultades ) + "%");
		System.out.println("REGULARES    : " + (conteo_dificultades * 100)/(conteo_dificultades+conteo_reprobados+conteo_aprobados ) + "%");

	}

}


